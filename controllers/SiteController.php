<?php

namespace app\controllers;

use Yii;
use yii\web\Controller;


class SiteController extends Controller
{   
    public $enableCsrfValidation = false;

    public function behaviors()
    {   

        return [
        ];

    }


    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    public function actionIndex()
    {   
        $array = \app\models\Faces::find()->orderBy('f_id DESC')->all();

        $model = new \app\models\Faces();

        if($model->load(Yii::$app->request->post())){
            $model->name = $_POST['Faces']['name'];
            $model->surname = $_POST['Faces']['surname'];
            $model->fathersname = $_POST['Faces']['fathersname'];
            $model->birthdate = $_POST['Faces']['birthdate'];
            $model->fullname = $model->surname." ".$model->name." ".$model->fathersname;
            $model->save();
            return $this->refresh();
        }
        return $this->render('faces', ['array'=>$array, 'model'=>$model]);
            
    }

    public function actionKids()
    {   
        $array = \app\models\Kids::find()->joinWith('faces')->orderBy('k_id DESC')->all();

        $model = new \app\models\Kids();

        if($model->load(Yii::$app->request->post())){
            $model->name = $_POST['Kids']['name'];
            $model->surname = $_POST['Kids']['surname'];
            $model->fathersname = $_POST['Kids']['fathersname'];
            $model->birthdate = $_POST['Kids']['birthdate'];
            $model->f_id = $_POST['Kids']['f_id'];
            $model->fullname = $model->surname." ".$model->name." ".$model->fathersname;
            $model->save();
            return $this->refresh();
        }
        return $this->render('kids', ['array'=>$array, 'model'=>$model]);
            
    }









}
