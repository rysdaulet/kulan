<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "faces".
 *
 * @property integer $f_id
 * @property string $fullname
 * @property string $name
 * @property string $surname
 * @property string $fathersname
 * @property string $birthdate

 */
class Faces extends \yii\db\ActiveRecord
{   

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'faces';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'surname', 'fathersname', 'fullname'], 'string', 'max' => 255],
            [['name','surname','birthdate', 'fathersname'], 'required'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'fullname' => Yii::t('app', 'Полное имя'),
            'name' => Yii::t('app', 'Имя'),
            'surname' => Yii::t('app', 'Фамилия'),
            'fathersname' => Yii::t('app', 'Отчество'),
            'birthdate' => Yii::t('app', 'Дата рождения'),
        ];
    }
    
    
    
}
