<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "kids".
 *
 * @property integer $f_id
 * @property integer $k_id
 * @property string $fullname
 * @property string $name
 * @property string $surname
 * @property string $fathersname
 * @property string $birthdate

 */
class Kids extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'kids';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['fullname', 'name', 'surname', 'fathersname'], 'string', 'max' => 255],
            [['name','surname','birthdate', 'fathersname'], 'required'],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'fullname' => Yii::t('app', 'Полное имя'),
            'name' => Yii::t('app', 'Имя'),
            'surname' => Yii::t('app', 'Фамилия'),
            'fathersname' => Yii::t('app', 'Отчество'),
            'birthdate' => Yii::t('app', 'Дата рождения'),
        ];
    }

    public function getFaces() {
        return $this->hasOne(\app\models\Faces::className(), ['f_id' => 'f_id']);
    }
    
    
    
}
