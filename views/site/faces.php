<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\datetime\DateTimePicker;


$this->title = Yii::t('user', 'Faces');
?>

<div class="row">
          <div class="col-lg-12">
            <div class="page-header">
              <h1 id="typography"><?= Html::encode($this->title) ?></h1>
            </div>
          </div>
        </div>
<div class="row">

    <div class="col-md-12">

                <?php $form = ActiveForm::begin([
                    'id'          => 'faces-form',
                    'options'     => ['class' => 'form-horizontal'],
                    'fieldConfig' => [
                        'template'     => "{label}\n<div class=\"col-lg-8\">{input}</div>\n<div class=\"col-sm-offset-4 col-lg-8\">{error}\n{hint}</div>",
                        'labelOptions' => ['class' => 'col-lg-4 control-label'],
                    ],
                ]); ?>

                <?= $form->field($model, 'name') ?>
                <?= $form->field($model, 'surname') ?>
                <?= $form->field($model, 'fathersname') ?>
                <?= $form->field($model, 'birthdate')->textInput(['placeholder' => 'гггг-мм-дд']) ?>


                <div class="form-group">
                    <div class="col-lg-offset-4 col-lg-8">
                        <?= Html::submitButton(Yii::t('user', 'Добавить'), ['class' => 'btn btn-block btn-success']) ?><br>
                    </div>
                </div>

                <?php ActiveForm::end(); ?>
 
    </div>


    <div class="col-md-12">
        <div class="table-responsive">
            <table class="table table-hover">
                <thead>
                    <tr>
                        <th>ФИО</th>
                        <th>Имя</th>
                        <th>Фамилия</th>
                        <th>Отчество</th>
                        <th>Дата рождения</th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach ($array as $item): ?>
                    <tr>
                        <td><?php echo $item->fullname ?></td>
                        <td><?php echo $item->name ?></td>
                        <td><?php echo $item->surname ?></td>
                        <td><?php echo $item->fathersname ?></td>
                        <td><?php echo $item->birthdate ?></td>
                    </tr>
                <?php endforeach ?>
                </tbody>
            </table>
        </div>
    </div>
</div>
